@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row pb-2">
        <div class="col-md-8">
            <h4>My Conversions</h4>
        </div>
    </div>

    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Conversions</li>
    </ol>
    </nav>

    <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th class="th-sm">Id
            </th>
            <th class="th-sm">Uploaded File
            </th>
            <th class="th-sm">Converted File
            </th>
          </tr>
        </thead>
        <tbody>
            @foreach ($conversions as $conversion)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><a href="{{ $conversion->getFirstMediaUrl('files') }}">Download</a></td>
                <td> <a href="{{ $conversion->getFirstMediaUrl('converted') }}">Download</a></td>
            </tr>
            @endforeach
        </tbody>
      </table>
      {{ $conversions->links() }}
</div>
@section('script')
@endsection

@endsection
