@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.css">
    <style>
        .alert p{
            margin-bottom: 0rem !important;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="alert alert-danger alert-dismissible" role="alert" style="display: none;">
        <p id="message"></p>
    </div>

    <div class="text-center">
        <h3>Convert Word to PDF</h3><br>
        <p>* Please chose your file or drag and drop which you want to convert.</p>
    </div>

    <div class="text-center">
        <form method="post" action="{{ route('file-upload.store') }}" enctype="multipart/form-data" class="dropzone" id="dropzone">
            @csrf
        </form>
    </div>
    <div class="text-center pt-5 pb-3">
        <h4 >How to convert Word to PDF online:</h4>
    </div>

    <div class="row text-center">

        <div class="col-md-3">
            <img src="{{ asset('img/1.svg') }}" alt="">
            <p>1. To begin, drag and drop your DOC or DOCX file in the Word to PDF converter.</p>
        </div>
        <div class="col-md-3">
            <img src="{{ asset('img/2.svg') }}" alt="">
            <p>2. The conversion to PDF should take place right away.</p>
        </div>
        <div class="col-md-3">
            <img src="{{ asset('img/3.svg') }}" alt="">
            <p>3. Feel free to compress, edit or modify your file further.</p>
        </div>
        <div class="col-md-3">
            <img src="{{ asset('img/4.svg') }}" alt="">
            <p>4. Click the download button to save your new PDF.</p>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <div class="text-center  pt-5" id="loader" style="display: none">
            <img class="" src="{{ asset('img/1486.gif') }}" alt="">
            <h3>Converting...</h3>
        </div>
        <div class="text-center" id="download_section" style="display: none; padding-top: 5em;">
           <h3>Download the converted file from <a href="" id="download" target="_blank">here</a>.</h3>
        </div>
    </div>

</div>

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/dropzone.js"></script>
<!-- Font Awesome JS -->
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

<script type="text/javascript">
    Dropzone.options.dropzone =
    {
        maxFilesize: 12,
        resizeQuality: 1.0,
        acceptedFiles: ".doc,.docx",
        addRemoveLinks: true,
        timeout: 60000,
        processing: function()
        {
            $('.alert').hide();
            $('#loader').show()
            $('html, body').animate({
                scrollTop: $("#loader").offset().top
            }, 2000);
        },
        removedfile: function(file)
        {
            $('.alert').hide();
            $('#download_section').hide()
            var fileRef;
            return (fileRef = file.previewElement) != null ?
            fileRef.parentNode.removeChild(file.previewElement) : void 0;

        },
        success: function (file, response) {
            console.log(response);
            $('#loader').hide()
            $('#download_section').show()
            $('#download').attr('href',response.success)
        },
        error: function (file, response) {
            if(response.message) {
                $('.alert').show();
                $('#message').text(response.message);
                $('html, body').animate({
                    scrollTop: $("#app").offset().top
                }, 2000);
                $('#loader').hide();
                $('#download_section').hide()
            }
        }
    };

</script>

@endsection
@endsection
