@extends('layouts.app')

@section('css')
    <style>
        .card-header{
            background-color: unset !important;
            border-bottom: unset !important;
            text-align: center;
            font-size: 1.2rem;
        }
        .alert p{
            margin-bottom: 0rem !important;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="alert alert-danger alert-dismissible" role="alert" style="display: none;">
        <p id="message"></p>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-5">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="" id="loginForm">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">

                                <button type="submit" class="btn btn-primary" id="login_button">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0 float-right">
                            <a class="btn btn-link" href="{{ route('register') }}">
                                Need an account?
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // Attach a submit handler to the form
            $( "#loginForm" ).submit(function( event ) {

            // Stop form from submitting normally
            event.preventDefault();

            var $this = $(this);
            console.log('logging...');
            $('#login_button').text('Logging in...');
            // Get some values from elements on the page:
            $.ajax({
                    type: 'POST',
                    url: '{{ url('login') }}',
                    data: $this.serializeArray(),
                    dataType: $this.data('type'),
                    success: function (response) {
                        if(response.token) {
                            window.location.replace("/");
                        }
                    },
                    error: function (jqXHR) {
                        var response = $.parseJSON(jqXHR.responseText);
                        $('#login_button').text('Login');
                        if(response.message) {
                            $('.alert').show();
                            $('#message').text(response.message);
                        }
                    }
                });
            });
        });
    </script>
@endsection
@endsection
