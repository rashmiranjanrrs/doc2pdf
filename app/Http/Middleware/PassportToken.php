<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class PassportToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = isset($_COOKIE["passport_token"])?$_COOKIE["passport_token"]:"";
        // $token = Cookie::get('passport_token');
        $request->headers->set("Authorization", "Bearer $token");
        $response = $next($request);
        return $response;
    }
}
