<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use CloudConvert\Laravel\Facades\CloudConvert;
use CloudConvert\Models\Job;
use CloudConvert\Models\Task;
use Illuminate\Support\Facades\Storage;
use App\Models\UserConversion;

class FileUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conversions = UserConversion::whereUserId(auth()->id())->paginate(5);
        return view('conversion',compact('conversions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file');
        $random = mt_rand(8,15);

        // $file->move(public_path('docs'), $fileName);

        $fileUpload = auth()->user()->conversions()->create([
            'filename'  =>  $random,
            'status'    =>  0
        ]);

        $fileUpload->addMedia($file)->toMediaCollection('files');

        $url = $this->convert($fileUpload);
        return response()->json(['success' => UserConversion::find($fileUpload->id)->getFirstMediaUrl('converted')]);
    }

    public function convert($fileUpload)
    {
        $job = (new Job())
            ->addTask(new Task('import/upload','upload-my-file'))
            ->addTask(
                (new Task('convert', 'convert-my-file'))
                    ->set('input', 'upload-my-file')
                    ->set('output_format', 'pdf')
            )
            ->addTask(
                (new Task('export/url', 'export-my-file'))
                    ->set('input', 'convert-my-file')
            );

        CloudConvert::jobs()->create($job);

        $uploadTask = $job->getTasks()->whereName('upload-my-file')[0];

        $inputStream = fopen($fileUpload->getFirstMedia('files')->getPath(), 'r');

        CloudConvert::tasks()->upload($uploadTask, $inputStream);


        CloudConvert::jobs()->wait($job); // Wait for job completion

        foreach ($job->getExportUrls() as $file) {
            $fileUpload->addMediaFromUrl($file->url)->toMediaCollection('converted');
            $fileUpload->status = 1;
            $fileUpload->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
