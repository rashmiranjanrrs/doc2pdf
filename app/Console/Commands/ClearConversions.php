<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UserConversion;
use Illuminate\Support\Carbon;

class ClearConversions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:conversions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove conversions older than 30days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->confirm('Do you wish to continue?')) {
            $oldConversions = UserConversion::where('created_at', '<=', Carbon::now()->subDays(30)->toDateTimeString())->get();
            foreach ($oldConversions as $key => $oldConversion) {
                $this->info("Removing: {$oldConversion->filename}!");
                $oldConversion->clearMediaCollection();
                $oldConversion->delete();
            }
        }

    }
}
