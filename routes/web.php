<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FileUploadController;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('file-upload', FileUploadController::class);
    Route::get('my-conversions', [FileUploadController::class,'index'])->name('my.conversion');
});

Route::post('logout', function () {
    auth()->logout();
    return redirect('/login')->withCookie(\Cookie::forget('passport_token'));
})->name('logout');


Route::get('passport-install', function () {
    Artisan::call('passport:install');
    return 'passport installed';
});
